window.onload = function () {
    var c = document.getElementById("duk");
    c.width = 600;
    c.height = 300;
    var ctx = c.getContext("2d");
    var raf;

    // Linjer färg, tjocklek och ändstil
    ctx.strokeStyle = "#00eb2d";
    ctx.lineWidth = 17;
    ctx.lineCap = "round";

    // Skapa ett objekt boll
//    var boll = {
//        x: 100,
//        y: 100,
//        radius: 25,
//        color: 'blue',
//        rita: function() {
//            ctx.beginPath();
//            ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2);
//            ctx.closePath();
//            ctx.fillStyle = this.color;
//            ctx.fill();
//        }
//    };
//
//    boll.rita();

    // Skapa en boll med hastigheter
    var boll = {
        x: 100,
        y: 100,
        vx: 5,
        vy: 2,
        radius: 25,
        color: 'blue',
        rita: function() {
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
            ctx.closePath();
            ctx.fillStyle = this.color;
            ctx.fill();
        }
    };

    // Rita ut bollen
    function rita() {
        ctx.clearRect(0, 0, c.width, c.height);
        boll.rita();
        boll.x += boll.vx;
        boll.y += boll.vy;
        raf = window.requestAnimationFrame(rita);
    }

    // Lyssna på mouseover
    c.addEventListener('mouseover', function(e) {
        raf = window.requestAnimationFrame(rita);
    });

    // Lyssna på mouseout
    c.addEventListener('mouseout', function(e) {
        raf = window.requestAnimationFrame(rita);
    });

    boll.rita();
}
