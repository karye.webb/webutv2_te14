// Allmäna variabler
var c, ctx, interval;

// Figurens position
snakePartsX = new Array();
snakePartsY = new Array();

// Ormens storlek
var snakeRadius = 7;
var snakeLength = 50;

// Ormens hastighet
var snakeSpeed = 4;
var snakeSpeedX = snakeSpeed;
var snakeSpeedY = 0;

// Ormens startposition
snakePartsX[0] = 0;
snakePartsY[0] = snakeRadius;

// Mat
var foodX, foodY, foodRadius = 3;

function run() {
    c = document.getElementById("myCanvas");
    ctx = c.getContext("2d");

    // Startposition för maten
    newFood();

    // Upprepa var 30ms
    interval = window.setInterval(gameLoop, 30);
}

// Spelmotorn
function gameLoop() {
    if (isCollidingTail()) {
        window.clearInterval(interval);
        gameOver();
    } else {

        // Sudda canvas
        ctx.clearRect(0, 0, c.width, c.height);

        // Rita ut ormen alla delar
        for (var i = 0; i < snakePartsX.length; i++) {
            ctx.beginPath();
            ctx.arc(snakePartsX[i], snakePartsY[i], snakeRadius, 0, 2 * Math.PI);
            ctx.fill();
        }

        // Rita ut maten
        ctx.beginPath();
        ctx.arc(foodX, foodY, foodRadius, 0, 2 * Math.PI);
        ctx.fill();

        collidingFood();

        console.log(snakePartsX + " " + snakePartsY);
        updatePositions();
    }
}

// Ändrar ormens position
function updatePositions() {

    // Lägg till ormdel i arrayerna
    snakePartsX.unshift(snakePartsX[0] + snakeSpeedX);
    snakePartsY.unshift(snakePartsY[0] + snakeSpeedY);

    // Om ormen längre än snakeLenght, ta bort sista delen
    if (snakePartsX.length > snakeLength) {
        snakePartsX.pop();
        snakePartsY.pop();
    }
}

function isCollidingTail() {
    for (var i = 9; i < snakePartsX.length; i++) {
        if (Math.abs(snakePartsX[0] - snakePartsX[i]) < snakeRadius * 2 &&
            Math.abs(snakePartsY[0] - snakePartsY[i]) < snakeRadius * 2)
            return true;
    }
    return false;
}

function gameOver() {
    ctx.font = "50px Sans-serif";
    ctx.fillText("Game over", 100, 100);
}

function newFood() {
    foodX = Math.round(Math.random() * c.width);
    foodY = Math.round(Math.random() * c.height);
}

function collidingFood() {
    if (Math.abs(snakePartsX[0] - foodX) < snakeRadius + foodRadius &&
            Math.abs(snakePartsY[0] - foodY) < snakeRadius + foodRadius) {
        snakeLength += 10;
        newFood();
    }
}

// Lyssna på tangentnedtryckningar
function keyDown(e) {
    switch (e.keyCode) {
    case 37: // Vänster
        snakeSpeedX = -snakeSpeed;
        snakeSpeedY = 0;
        break;

    case 39: // Höger
        snakeSpeedX = snakeSpeed;
        snakeSpeedY = 0;
        break;

    case 38: // Upp
        snakeSpeedY = -snakeSpeed;
        snakeSpeedX = 0;
        break;

    case 40: // Ned
        snakeSpeedY = snakeSpeed;
        snakeSpeedX = 0;
        break;
    }
}
