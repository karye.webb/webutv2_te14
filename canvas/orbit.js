window.onload = function () {
    var grad = Math.PI / 180;
    var c = document.getElementById("duk");
    c.width = 1000;
    c.height = 600;
    var ctx = c.getContext("2d");

    // Skapa bildobjekten
    var sun = new Image();
    var moon = new Image();
    var earth = new Image();

    // Laddat bildobjekten med grafik
    sun.src = 'images/Canvas_sun.png';
    moon.src = 'images/Canvas_moon.png';
    earth.src = 'images/Canvas_earth.png';

    // Rita ut bilden när grafiken laddats klart
//    sun.onload = function () {
//        ctx.drawImage(sun, 0, 0);
//    }

    // Förflytta koordinatsystemet och rita en rektangel
//    ctx.translate(100, 100);
//    ctx.fillRect(70, 0, 100, 30);
//    ctx.translate(100, 100);
//    ctx.fillRect(70, 0, 100, 30);

    // Rotera koordinatsystemet och rita en rektangel
//    ctx.rotate(30 * grad);
//    ctx.fillRect(70, 0, 100, 30);
//    ctx.rotate(30 * grad);
//    ctx.fillRect(70, 0, 100, 30);

    // Rita ut var 50 millisekund
//    var i = 0;
//    setInterval( function() {
//        ctx.translate(100, 100);
//        ctx.rotate(i*grad);
//        ctx.fillRect(70, 0, 100, 30);
//        console.log('grader='+i*grad);
//        i++;
//        ctx.translate(-100, -100);
//    }, 50);

    // Rotera 1 grad och rita ut jorden
    var i = 0;
    function earthPath() {

        // Rita ut bakgrunden med solen
        ctx.drawImage(sun, 0, 0, 600, 600);

        //Lagra koordinatsystemet läge
        ctx.save();

        // Flyttar på koordinatsystem
        ctx.translate(300, 300);

        // Roterar jordens position
        ctx.rotate(i * grad / 10);
        ctx.drawImage(earth, 120, 120);

        // Rita ut månen
        moonPath(i);

        // Återställer koordinatsystemet
        ctx.restore();

        // Ökar vinkel med en grad
        i++;

         window.requestAnimationFrame(earthPath);
    }

    function moonPath(i) {
        //Lagra koordinatsystemet läge
        ctx.save();

        // Flyttar på koordinatsystem
        ctx.translate(132, 132);

        // Roterar jordens position
        ctx.rotate(i * grad);
        ctx.drawImage(moon, 15, 15);

        // Återställer koordinatsystemet
        ctx.restore();
    }

    window.requestAnimationFrame(earthPath);
}
